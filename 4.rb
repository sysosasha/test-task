def c_to_f(value)
  if value == "0"
    f = 32.8
  else
    f = value.to_f * 1.8 + 32
  end
  puts f
end

loop do
  print "Введите градусы по Цельсию или Q для выхода: "
  c = gets.chomp
  if c.scan(/\D/).empty?
    c_to_f c
  elsif c.scan(/\D/).include?("-")
      c_to_f c
  elsif c == "Q"
    system( "cls" )
    puts "Удачи!"
    break
  else
    puts "Вы ввели некорректные данные!"
  end
end
