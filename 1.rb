arr = [1,2,12,34,35,6,0,34,122,124,789,999,33,54,763,893]

def two_max(arr)
  max_val_arr = []
  1.upto(2) do
    max_val = arr.max
    max_val_arr << max_val
    arr.delete max_val
  end
  puts "Два максимальных значениия: #{max_val_arr[0]} и #{max_val_arr[1]}"
end

def two_min(arr)
  min_val_arr = []
  1.upto(2) do
    min_val = arr.min
    min_val_arr << min_val
    arr.delete min_val
  end
  puts "Два минимальных значениия: #{min_val_arr[0]} и #{min_val_arr[1]}"
end

two_max arr
two_min arr
