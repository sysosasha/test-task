arr = [nil, 2, :foo, "bar", "foo", "apple", "orange", :orange, 45, nil, :foo, :bar, 25, 45, :apple, "bar", nil]
hh = {}

arr.uniq.each {|value| hh[value] = 0}

hh.each_key {|key| arr.each {|value| hh[key] += 1 if key == value}}

puts hh.inspect
