def traffic_light color
  if color == "red"
    puts "стоять"
  elsif color == "yellow"
    puts "ждать"
  elsif color == "green"
    puts "идти"
  else
    puts "Вы ввели некорректные данные!"
  end
end

loop do
  print "Введите цвет red/yellow/green или Q для выхода: "
  color = gets.chomp
  break if color == "Q"
  traffic_light color
end
