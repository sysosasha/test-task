arr = [{a: 1, b: 2, c: 45}, {d: 123, c: 12}, {e: 87}]
all_keys = []
all_val = []

arr.each do |hh|
  hh.each do |key,value|
    all_keys << key
    all_val << value
  end
end

puts "Массив всех ключей: #{all_keys.inspect}"
puts "Массив всех значений: #{all_val.inspect}"
puts "Сумма всех значений: #{all_val.sum}"
